﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie5
{
    class Program
    {
        // Napisz program symulujący grę w 21 (uczestnik zbiera punkty do momentu, aż uzyska liczbę najbliższą 21,
        // jeśli ją przekroczy przegrywa – można grać w parach w ławce).
        // Dopisać zabezpieczenie, aby w przypadku wpisania innego znaku niż t/n program wyświetlał komunikat
        static void Main(string[] args)
        {
            int uzyskanyWynik = 0;
            Random losowaLiczba = new Random();
            const int maxWynik = 21;

            do
            {
                Console.WriteLine("Uzbierałeś {0} punktów. Chcesz kontynuowac? (t / n)",uzyskanyWynik);
                char wybranyZnak = char.Parse(Console.ReadLine());
                if (wybranyZnak == 'n')
                {
                    Console.WriteLine("Zakonczyłeś zbieranie punktów");
                    break;
                }
                else if (wybranyZnak == 't')
                { 
                    int wylosowanaLiczba = losowaLiczba.Next(1, 7);
                    uzyskanyWynik += wylosowanaLiczba;
                }
                else
                {
                    Console.WriteLine("{0} - nie jest poprawnym znakiem! Wybierz 't' lub 'n'", wybranyZnak);
                }
            }
            while (uzyskanyWynik < maxWynik);

            if (uzyskanyWynik > maxWynik)
            {
                Console.WriteLine("Uzbierałeś za dużo!");
            }
            else
            {
                Console.WriteLine("Uzbierałeś {0} punktów", uzyskanyWynik);
            }
            Console.ReadKey();

        }
    }
}
